## About

My personal blog. Home of [gjhenrique.com](https://gjhenrique.com)

Based on the wonderful Jekyll theme [Emerald](https://github.com/KingFelix/emerald)
